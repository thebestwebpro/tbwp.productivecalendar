<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Type;
use Bitrix\Main\Entity\Query;
use Tbwp\ProductiveCalendar\CalendarTable;

require_once($_SERVER['DOCUMENT_ROOT'] .'/bitrix/modules/main/include/prolog_admin_before.php');

Loc::loadMessages(__FILE__);

global $USER, $APPLICATION, $DB;

$MODULE_ID = 'tbwp.productivecalendar';

if (!Loader::includeModule($MODULE_ID)) {
    return false;
}

$APPLICATION->SetTitle(Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_TITLE'));

$aTabs[]=array("DIV" => "step1", "TAB" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_TAB_TITLE"), "ICON"=>"main_user_edit", "TITLE"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_TAB_TITLE"));

$tabControl = new \CAdminTabControl("tabControl", $aTabs);

$request = \Bitrix\Main\Context::getCurrent()->getRequest();

$dt=\Carbon\Carbon::now();

$STEP = IntVal($request->get('step'));

if ($request->isPost() && check_bitrix_sessid() && $STEP==1)
{
    $tableName=Tbwp\ProductiveCalendar\CalendarTable::getTableName();
    $connection = \Bitrix\Main\Application::getConnection();

    $year=$request->get('year');
    if($request->get('clear_old')=='Y'){
        $connection->truncateTable($tableName);
    }

    $obj=new Tbwp\ProductiveCalendar\Parser\Consultant();
    $result=$obj->exec($year);
    if(!empty($obj->getError()))
        $message =$obj->getError();
    try{

        foreach ($result[$year] as $month=>$days){
            $arItems=[];
            if($request->get('clear_old')<>'Y'){
                $userQuery = new Query(CalendarTable::getEntity());
                $userQuery->setSelect(['*']);
                $userQuery->setOrder(array('DATE' => 'asc'));
                $userQuery->setFilter(array('DATE' => array_map(function($item){return new Type\Date($item, 'Y-m-d');},array_keys($days))));
                $rsOldItems = $userQuery->exec();
                while($arOldItem=$rsOldItems->fetch()){
                    unset($days[$arOldItem['DATE']->format('Y-m-d')]);
                }
            }

            array_map(function($key,$item) use (&$arItems){
                $arItems[]=[
                    'DATE'=>new Type\Date($key, 'Y-m-d'),
                    'TYPE'=>$item['TYPE'],
                    'DESCRIPTION'=>$item['DESCRIPTION']
                ];
            },  array_keys($days), array_values($days));

            foreach ($arItems as $insertItem){
                $resultAdd=CalendarTable::add($insertItem);
                if(!$resultAdd->isSuccess())
                    throw new \Exception("Произошла ошибка при добавлении ".implode(PHP_EOL,$resultAdd->getErrorMessages()));
            }
            unset($arItems,$insertItem);
        }

        $message= new \CAdminMessage(array("MESSAGE"=>'Операция успешно выполнена', "TYPE"=>"OK"));
    }catch (\Exception $e){
        $message =new \CAdminMessage($e->getMessage());
    }

}

$aMenu=[];

$aMenu[] = [
    'TEXT'  => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_RETURN'),
    'TITLE' => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_RETURN'),
    'LINK'  => 'tbwp_productive_calendar_list.php?lang='.LANGUAGE_ID,
    'ICON'  => 'btn_list',
];

$context = new CAdminContextMenu($aMenu);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

$context->Show();

if($message)
    echo $message->Show();

$tabControl->Begin();
?>
<form method="POST" action="<?=$APPLICATION->GetCurPage();?>?lang=<?=LANGUAGE_ID; ?>" ENCTYPE="multipart/form-data" name="dataload">
    <input type="hidden" name="step" value="1">
    <?=bitrix_sessid_post()?>
    <input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
    <?$tabControl->BeginNextTab();?>
    <tr>
        <td width="40%" class="adm-detail-valign-top"><b><?=Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_CLEAR_OLD")?></b></td>
        <td width="60%">
            <input type="checkbox" name="clear_old" value="Y" <?=$request->get('clear_old')=='Y' ? "checked": ""?> />
        </td>
    </tr>
    <tr>
        <td width="40%" class="adm-detail-valign-top"><b><?=Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_YEAR")?></b></td>
        <td width="60%">
            <input type="text" name="year" value="<?=$request->get('year') ?? $dt->year?>"  />
        </td>
    </tr>
    <?$tabControl->Buttons();?>
        <input type="submit" value="<?echo GetMessage("TBWP_PRODUCTIVE_CALENDAR_BTN_START")?>" name="submit_btn" class="adm-btn-save">
    <?$tabControl->End();?>
</form>