<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader,
    Bitrix\Main\Type;
use Tbwp\ProductiveCalendar\CalendarTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

Loc::loadMessages(__FILE__);

global $USER, $APPLICATION, $DB;

$MODULE_ID = 'tbwp.productivecalendar';

if (!Loader::includeModule($MODULE_ID)) {
    return false;
}

$POST_RIGHT = $APPLICATION->GetGroupRight($MODULE_ID);
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

$action = isset($_REQUEST['action']) ? htmlspecialcharsbx($_REQUEST['action'])
    : 'add';

// get row
$row = null;

$sTableID=CalendarTable::getTableName();

// выборка данных
if($_REQUEST['ID']>0)
{
    $result = CalendarTable::GetByID(intval($_REQUEST['ID']));
    if($row=$result->fetch()){
        $ID=$row['ID'];
        $bVarsFromForm = true;
    }else{
        $errors[] = "Not found";
    }
}else{
    $row=$_REQUEST;
}

if (intval($_REQUEST['ID']) > 0) {
    $APPLICATION->SetTitle(Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_TITLE_EDIT',['#DATE#' => $row['NOT_WORK_DATE']]));
} else {
    $APPLICATION->SetTitle(Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_TITLE'));
}

$aTabs[]=array("DIV" => "step1", "TAB" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_TAB_TITLE"), "ICON"=>"main_user_edit", "TITLE"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_TAB_TITLE"));

$tabControl = new \CAdminForm('tbwp_productive_calendar_item', $aTabs);


// delete action
if ($action == 'delete') {
    CalendarTable::delete($ID);
    LocalRedirect('tbwp_productive_calendar_list.php?lang='.LANGUAGE_ID);
}

// save action
if ((mb_strlen($save) > 0 || mb_strlen($apply) > 0) && $REQUEST_METHOD == 'POST'
    && check_bitrix_sessid()
) {

    $arFields = [
        'DATE'=>Type\Date::createFromText($_REQUEST['DATE']),
        'TYPE'=>$_REQUEST['TYPE'],
        'DESCRIPTION'=>$_REQUEST['DESCRIPTION']
    ];

    /** @param Bitrix\Main\Entity\AddResult $result */
    if ($ID > 0) {
        $result = CalendarTable::update($ID, $arFields);
    } else {
        $result = CalendarTable::add($arFields);
        $ID = $result->getId();
    }
    if ($result->isSuccess()) {
        if (mb_strlen($save) > 0) {
            LocalRedirect('tbwp_productive_calendar_list.php?lang='.LANGUAGE_ID);
        } else {
            LocalRedirect( $APPLICATION->getCurPageParam("ID={$ID}&".$tabControl->ActiveTabParam(), ['action','ID']));
        }
    } else {
        $errors = $result->getErrorMessages();
    }
}

$aMenu=[];

$aMenu[] = [
    'TEXT'  => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_RETURN'),
    'TITLE' => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_RETURN'),
    'LINK'  => 'tbwp_productive_calendar_list.php?lang='.LANGUAGE_ID,
    'ICON'  => 'btn_list',
];
$aMenu[] = [
    'TEXT'  => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_ADD'),
    'TITLE' => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_ADD'),
    'LINK'  => $APPLICATION->getCurPageParam('ID=0', [
        'action',
        'ID',
    ]),
    'ICON'  => 'btn_new',
];
$aMenu[] = [
    'TEXT'  => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_DELETE'),
    'TITLE' => Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_MENU_DELETE'),
    'LINK'  => $APPLICATION->getCurPageParam('action=delete', ['action']),
    'ICON'  => 'delete',
];

CJSCore::Init(array('date'));

$context = new CAdminContextMenu($aMenu);

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_admin_after.php');

$context->Show();

if (!empty($errors)) {
    $bVarsFromForm = true;
    \CAdminMessage::ShowMessage(join("\n", $errors));
} else {
    $bVarsFromForm = false;
}

$tabControl->BeginPrologContent();
$tabControl->EndPrologContent();
$tabControl->BeginEpilogContent();
?>
<?= bitrix_sessid_post() ?>
<input type="hidden" name="ID" value="<?= htmlspecialcharsbx(!empty($row) ? $row['ID'] : '') ?>">
<input type="hidden" name="lang" value="<?= LANGUAGE_ID ?>">
<input type="hidden" name="action" value="<?= $action ?>">
<?php $tabControl->EndEpilogContent();

$tabControl->Begin(['FORM_ACTION' => $APPLICATION->GetCurPage().'?ID='.IntVal($ID).'&lang='.LANGUAGE_ID]);

$tabControl->BeginNextFormTab();
if ($ID > 0){
    $tabControl->BeginCustomField('NAME_NOTE', '');
    ?>
    <tr class="tabcontent">
        <td width="40%"><?=Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_F_DATE')?></td>
        <td width="60%">
           <?=$row['DATE']?>
        </td>
    </tr><?php
    $tabControl->EndCustomField("NAME_NOTE");

}else{
    $tabControl->AddCalendarField('DATE',Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_F_DATE'),$row['DATE'],true);
}
$type=[
    "weekend"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_WEEKEND"),
    "holiday"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_HOLIDAY")
];
$tabControl->AddDropDownField('TYPE',Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_F_TYPE'),true,$type,$row['TYPE']);

$tabControl->AddTextField('DESCRIPTION',Loc::getMessage('TBWP_PRODUCTIVE_CALENDAR_F_DESCRIPTION'),$row['DESCRIPTION'],['cols' => 50,'rows'=>10]);

$tabControl->Buttons([
    'disabled' => false,
    'back_url' => 'tbwp_productive_calendar_list.php&lang='.LANGUAGE_ID,
]);

$tabControl->Show();