<?php

use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\Entity\Query;
use Tbwp\ProductiveCalendar\CalendarTable;

require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");

Loc::loadMessages(__FILE__);

global $USER, $APPLICATION, $DB;

$MODULE_ID = 'tbwp.productivecalendar';

if (!Loader::includeModule($MODULE_ID)) {
    return false;
}

$POST_RIGHT = $APPLICATION->GetGroupRight($MODULE_ID);
if ($POST_RIGHT == "D")
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));


$sTableID = CalendarTable::getTableName();

$oSort = new CAdminSorting($sTableID, "DATE", "asc");
$lAdmin = new CAdminUiList($sTableID, $oSort);

$arFilterFields = array(
    "find_id",
    "find_date",
    "find_type",
);

$lAdmin->InitFilter($arFilterFields);

function CheckFilter($FilterArr)
{
    global $strError;
    foreach ($FilterArr as $f)
        global ${$f};

    $str = "";
    /*if (strlen(trim($find_date_create)) > 0 || strlen(trim($find_date_update)) > 0) {
        $date_1_ok = false;
        $date1_stm = MkDateTime(FmtDate($find_date_create, "D.M.Y"), "d.m.Y");
        $date2_stm = MkDateTime(FmtDate($find_date_update, "D.M.Y") . " 23:59", "d.m.Y H:i");
        if (!$date1_stm && strlen(trim($find_date_create)) > 0)
            $str .= Loc::getMessage("MAIN_WRONG_TIMESTAMP_FROM") . "<br>";
        else $date_1_ok = true;
        if (!$date2_stm && strlen(trim($find_date_update)) > 0)
            $str .= Loc::getMessage("MAIN_WRONG_TIMESTAMP_TILL") . "<br>";
        elseif ($date_1_ok && $date2_stm <= $date1_stm && strlen($date2_stm) > 0)
            $str .= Loc::getMessage("MAIN_FROM_TILL_TIMESTAMP") . "<br>";
    }*/

    $strError .= $str;
    if (strlen($str) > 0) {
        global $lAdmin;
        $lAdmin->AddFilterError($str);
        return false;
    }

    return true;
}

$arFilter = array();
if (CheckFilter($arFilterFields)) {
    $arFilter = array(
        "ID" => $find_id,
        "DATE" => $find_date,
        "TYPE" => $find_type,
    );
}
$listServices = array();
$rsServices = CalendarTable::getList();
while ($service = $rsServices->fetch())
    $listServices[$service["ID"]] = $service["NAME"];

$filterFields = array(
    array(
        "id" => "ID",
        "name" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_ID"),
        "filterable" => "",
    ),
    array(
        "id" => "DATE",
        "name" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_DATE"),
        "type" => "date",
    ),
    array(
        "id" => "TYPE",
        "name" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_TYPE"),
        "type" => "list",
        "items"=>[
            "weekend"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_WEEKEND"),
            "holiday"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_HOLIDAY")
        ]
    ),
);

$arFilter = array();
$lAdmin->AddFilter($filterFields, $arFilter);

if (($arID = $lAdmin->GroupAction())) {
    if (!is_array($_REQUEST['ID']))
        $arID[] = $_REQUEST['ID'];
    else
        $arID = $_REQUEST['ID'];

    if ($lAdmin->IsGroupActionToAll())
    {
        $arID =[];
        $rsResult = CalendarTable::getList(array(
            'select' => array('ID'),
            'filter' => $arFilter
        ));
        $arAllItems = $rsResult->fetchAll();
        foreach ($arAllItems as $arAllItem)
            $arID[] = $arAllItem['ID'];
        unset($arAllItem, $arAllItems, $rsResult);
    }

    try {
        @set_time_limit(0);
        $DB->StartTransaction();

        switch ($_REQUEST['action']) {
            case "delete":
                foreach ($arID as $ID) {
                    $ID = intval($ID);
                    if ($ID <= 0)
                        continue;

                    $rsResult = CalendarTable::delete($ID);
                    if (!$rsResult->isSuccess())
                        throw new \Exception(implode('<br>', $rsResult->getErrorMessages()));
                }
                break;
        }

        $DB->Commit();
    }catch (\Exception $e){
        $DB->Rollback();
        $lAdmin->AddGroupError(Loc::getMessage("DELETE_ERROR") . $e->getMessage(), $ID);
    }
}

setHeaderColumn($lAdmin);
$nav = new PageNavigation("pages-user-admin");
$nav->setPageSize($lAdmin->getNavSize());
$nav->initFromUri();

$userQuery = new Query(CalendarTable::getEntity());
$listSelectFields = $lAdmin->getVisibleHeaderColumns();
if (!in_array("ID", $listSelectFields))
    $listSelectFields[] = "ID";

$userQuery->setSelect($listSelectFields);
$sortBy = strtoupper($by);
if (!CalendarTable::getEntity()->hasField($sortBy)) {
    $sortBy = "ID";
}
$sortOrder = strtoupper($order);
if ($sortOrder <> "DESC" && $sortOrder <> "ASC") {
    $sortOrder = "DESC";
}
$userQuery->setOrder(array($sortBy => $sortOrder));
$userQuery->countTotal(true);
$userQuery->setOffset($nav->getOffset());
$userQuery->setLimit($nav->getLimit());

$filterOption = new Bitrix\Main\UI\Filter\Options($sTableID);
$filterData = $filterOption->getFilter($filterFields);
if (!empty($filterData["FIND"])) {
    $userQuery->setFilter(\Bitrix\Main\UserUtils::getAdminSearchFilter(array("FIND" => $filterData["FIND"])));
}

/*if (isset($arFilter["TIMESTAMP_1"])) {
    $userQuery->where("DATE_CREATE", ">=", new DateTime($arFilter["TIMESTAMP_1"]));
}
if (isset($arFilter["TIMESTAMP_2"])) {
    $userQuery->where("TIMASTAMP_X", "<=", new DateTime($arFilter["TIMESTAMP_2"]));
}*/


$ignoreKey = array("NAME", "CHECK_SUBORDINATE", "CHECK_SUBORDINATE_AND_OWN", "NOT_ADMIN", "INTRANET_USERS", "GROUPS_ID",
    "KEYWORDS", "TIMESTAMP_1", "TIMESTAMP_2", "LAST_LOGIN_1", "LAST_LOGIN_2"
);
foreach ($arFilter as $filterKey => $filterValue) {
    if (!in_array($filterKey, $ignoreKey)) {
        $userQuery->addFilter($filterKey, $filterValue);
    }
}

$result = $userQuery->exec();

$nav->setRecordCount($result->getCount());
$lAdmin->setNavigation($nav, GetMessage("MAIN_USER_ADMIN_PAGES"), false);

while ($userData = $result->fetch()) {

    $itemId = $userData["ID"];
    $arActions = array();
    $row =& $lAdmin->addRow($itemId, $userData);

    $row->addViewField("ID", "<a href='tbwp_productive_calendar_item.php?lang=" . LANGUAGE_ID . "&ID=" . $itemId .
        "' title='" . GetMessage("MAIN_EDIT_TITLE") . "'>" . $itemId . "</a>");

    $row->addViewField("DATE", $userData['DATE']);

    $type=[
        "weekend"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_WEEKEND"),
        "holiday"=>Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_HOLIDAY")
    ];

    $row->addViewField("TYPE", $type[$userData['TYPE']]);

    $arActions[] = array(
        "ICON" => "delete",
        "TEXT" => GetMessage("MAIN_ADMIN_MENU_DELETE"),
        "ACTION" => "if(confirm('" . GetMessage('CONFIRM_DEL_USER') . "')) " . $lAdmin->actionDoGroup($itemId, "delete")
    );

    $row->addActions($arActions);
}

$ar = array(
    "delete" => true,
    "for_all" => true,
);

$lAdmin->AddGroupActionTable($ar);

$aContext = array();
$aContext[] = array(
    "TEXT"	=> Loc::getMessage("MAIN_ADD"),
    "TITLE"	=> Loc::getMessage("MAIN_ADD"),
    "ICON"	=> "btn_new",
    "LINK"=>"tbwp_productive_calendar_item.php?lang=".LANGUAGE_ID,
);
$aContext[] = array(
    "TEXT"	=> Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_BTN_UPDATE"),
    "TITLE"	=> Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_BTN_UPDATE"),
    "ICON"	=> "btn_new",
    "LINK"=>"tbwp_productive_calendar_update.php?lang=".LANGUAGE_ID,
);
$lAdmin->AddAdminContextMenu($aContext,false,false);

$lAdmin->CheckListMode();

$APPLICATION->SetTitle(Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_LIST_TITLE"));

require($_SERVER["DOCUMENT_ROOT"] . BX_ROOT . "/modules/main/include/prolog_admin_after.php");

$lAdmin->DisplayFilter($filterFields);
$lAdmin->DisplayList();

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");

function setHeaderColumn(CAdminUiList $lAdmin)
{
    $arHeaders = array(
        array("id" => "ID", "content" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_ID"), "sort" => "id", "default" => true,),
        array("id" => "DATE", "content" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_DATE"), "sort" => "DATE", "default" => true, "align" => "left"),
        array("id" => "TYPE", "content" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_TYPE"), "default" => true, "align" => "left"),
        array("id" => "DESCRIPTION", "content" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_F_DESCRIPTION"), "default" => true, "align" => "left"),
    );

    $lAdmin->addHeaders($arHeaders);
}


