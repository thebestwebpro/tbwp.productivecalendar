<?
$MESS["TBWP_PRODUCTIVE_CALENDAR_TITLE"] = "Добавление выходного дня";
$MESS["TBWP_PRODUCTIVE_CALENDAR_TITLE_EDIT"] = "Редактирование выходного дня #DATE#";
$MESS["TBWP_PRODUCTIVE_CALENDAR_TAB_TITLE"] = "Основные";
$MESS["TBWP_PRODUCTIVE_CALENDAR_MENU_RETURN"] = "Назад к списку";
$MESS["TBWP_PRODUCTIVE_CALENDAR_MENU_ADD"] = "Добавить";
$MESS["TBWP_PRODUCTIVE_CALENDAR_MENU_DELETE"] = "Удалить";

$MESS["TBWP_PRODUCTIVE_CALENDAR_F_DATE"] = "Дата";
$MESS["TBWP_PRODUCTIVE_CALENDAR_F_DESCRIPTION"] = "Описание";
$MESS["TBWP_PRODUCTIVE_CALENDAR_F_TYPE"] = "Тип выходного";
$MESS["TBWP_PRODUCTIVE_CALENDAR_F_WEEKEND"] = "Выходной";
$MESS["TBWP_PRODUCTIVE_CALENDAR_F_HOLIDAY"] = "Праздник";

?>