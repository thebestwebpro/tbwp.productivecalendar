<?php
namespace Tbwp\ProductiveCalendar;

use Bitrix\Main,
    Bitrix\Main\ORM\Fields;

class CalendarTable extends Main\ORM\Data\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName(): string
    {
        return 'tbwp_productive_calendar';
    }

    /**
     * @return array
     * @throws Main\SystemException
     */
    public static function getMap(): array
    {
        return array(
            new Fields\IntegerField(
                'ID',
                [
                    'primary'      => true,
                    'autocomplete' => true,
                ]
            ),
            new Fields\DatetimeField('DATE'),
            new Fields\StringField('TYPE',[
                'required' => true
            ]),
            new Fields\TextField('DESCRIPTION')
        );
    }


}