<?php
namespace Tbwp\ProductiveCalendar\Restriction;

use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Internals\Entity;
use Tbwp\ProductiveCalendar\Main;
use Tbwp\ProductiveCalendar\WorkCalendar as Carbon;

class RestrictionByProductiveCalendar extends \Bitrix\Sale\Delivery\Restrictions\Base {

    public static $easeSort = 100;

    public static function getClassTitle(): ?string
    {
        return Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_RESTRICTION_TITLE");
    }

    public static function getClassDescription(): ?string
    {
        return Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_RESTRICTION_DESCRIPTION");
    }

    /**
     * Проверяем
     * @param mixed $params
     * @param array $restrictionParams
     * @param int $serviceId
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\IO\InvalidPathException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public static function check($params, array $restrictionParams, $serviceId = 0): bool
    {
        require_once Main::GetPathModule(true).'/vendor/autoload.php';
        $currentDateTime=Carbon::now();

        return $currentDateTime->isWorkday();
    }

    /**
     * Получим из корзины данные для проверки
     * @param \Bitrix\Sale\Internals\Entity $entity
     * @return array|mixed
     * @throws \Bitrix\Main\ArgumentException
     */
    protected static function extractParams(Entity $entity):array
    {
        return array();
    }


    public static function getParamsStructure($entityId = 0):array
    {
        return array();
    }
}