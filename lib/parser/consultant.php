<?php
namespace Tbwp\ProductiveCalendar\Parser;

use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Web\Uri;
use Carbon\Carbon;

class Consultant{

    const URL="https://www.consultant.ru/law/ref/calendar/proizvodstvennye/";
    private const WEEKEND = 'weekend';
    private const HOLIDAY = 'holiday';

    protected string $error;

    /**
     * @param string $year
     * @return string
     */
    protected function makeUrlByYear(string $year): string
    {
        return self::URL.$year;
    }

    protected function check(): void
    {
        if(!class_exists('DOMDocument'))
            throw new \Exception('Не установлено расширение php-xml');
    }

    /**
     * @param string $year
     * @return array
     */
    public function exec(string $year): array
    {
        $arResult=[];
        try {
            $arResult=$this->parse($year);
            if(empty($arResult))
                throw new \Exception('Пустой результат');

        }catch (\Exception $e){
            $this->error=$e->getMessage();
        }
        return $arResult;
    }

    /**
     * @param string $year
     * @return array
     * @throws \Exception
     */
    protected function parse(string $year): array
    {
        $this->check();

        $url=$this->makeUrlByYear($year);

        $options = array(
            "redirect" => true,
            "redirectMax" => 5,
            "waitResponse" => true,
            "socketTimeout" => 30,
            "streamTimeout" => 60,
            "version" => HttpClient::HTTP_1_0,
        );
        $httpClient = new HttpClient($options);
        $httpClient->setHeader("Accept-language: ru",  "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/78.0.3904.108 Chrome/78.0.3904.108 Safari/537.36", true);

        $httpClient->query("GET", $url);
        if($httpClient->getStatus()<>200)
            throw new \Exception('Не удалось получить содержимое страницы '.$url);

        $html =$httpClient->getResult();
        $dom  = new \DOMDocument();
        @$dom->loadHTML($html);

        $xml       = simplexml_import_dom($dom);
        $rawMonths = $xml->xpath("//table[@class='cal']");
        $arResult=[];

        foreach ($rawMonths as $monthNumber => $rawMonth) {
            $month = $monthNumber + 1;

            $arDateDescr=$this->getWeekendDescription($month,$year);
            foreach ($rawMonth->tbody->tr as $tr) {
                foreach ($tr->td as $td) {
                    $dayNumber = (int) $td;
                    $dayStatus = (string) $td['class'];

                    if ($dayStatus == 'inactively') {
                        continue;
                    }

                    $dt = Carbon::create($year, $month, $dayNumber, 0, 0, 0);

                    $dayStatusArr=explode(' ',$dayStatus);
                    if($this->isWeekend($dayStatusArr)){
                        $arResult[$year][$month][$dt->toDateString()]=[
                            'TYPE'=>'weekend',
                            'DESCRIPTION'=>''
                        ];
                    }

                    if($this->isHoliday($dayStatusArr)){
                        $arResult[$year][$month][$dt->toDateString()]=[
                            'TYPE'=>'holiday',
                            'DESCRIPTION'=>array_key_exists($dt->toDateString(),$arDateDescr) ? $arDateDescr[$dt->toDateString()] : ''
                        ];
                    }

                }
            }
        }

        return $arResult;
    }

    /**
     * @param string $month
     * @param string $year
     * @return array
     */
    protected function getWeekendDescription(string $month, string $year): array
    {
        $arResult=[];
        try {
            $url=new Uri('https://kayaposoft.com/enrico/json/v2.0');
            $url->addParams([
                'action'=>'getHolidaysForMonth',
                'month'=>$month,
                'year'=>$year,
                'country'=>'rus',
                'holidayType'=>'public_holiday'
            ]);
            $options = array(
                "redirect" => true,
                "redirectMax" => 5,
                "waitResponse" => true,
                "socketTimeout" => 30,
                "streamTimeout" => 60,
                "version" => HttpClient::HTTP_1_0,
            );
            $httpClient = new HttpClient($options);
            $httpClient->query("GET", $url->getUri());
            $result=$httpClient->getResult();

            foreach (Json::decode($result) as $date){
                $dt = Carbon::create($date['date']['year'], $date['date']['month'], $date['date']['day'], 0, 0, 0);
                $dateDescr=null;
                foreach ($date['name'] as $langItem){
                    if($langItem['lang']==LANG)
                        $dateDescr=$langItem['text'];
                }
                $arResult[$dt->toDateString()]=$dateDescr;
            }
        }catch (\Exception $e){
            $this->error=$e->getMessage();
        }

        return $arResult;
    }

    /**
     * Выходной день.
     *
     * @param array $status
     * @return bool
     */
    protected function isHoliday(array $status): bool
    {
        return in_array(self::HOLIDAY, $status);
    }

    /**
     * Праздничный день.
     *
     * @param $status
     * @return bool
     */
    protected function isWeekend($status): bool
    {
        return in_array(self::WEEKEND, $status);
    }

    /**
     * @return string
     */
    public function getError(): string
    {
        return $this->error;
    }


}