<?php
namespace Tbwp\ProductiveCalendar;

require_once Main::GetPathModule(true).'/vendor/autoload.php';

use Carbon\Carbon;
use Bitrix\Main\Type;

class WorkCalendar extends Carbon
{

    /**
     * Добавить рабочий день к текущей дате
     */
    public function addWorkday():void
    {
        $this->addWorkdays(1);
    }

    /**
     * Добавить несколько рабочих дней к текущей дате
     *
     * @param int $count Количество добавляемых рабочих дней
     */
    public function addWorkdays(int $count):void
    {
        $count = abs($count);
        while ($count > 0) {
            $this->addDay();

            if ($this->isWorkday()) {
                $count--;
            }
        }
    }

    /**
     * Вычесть рабочий день от текущей даты
     */
    public function subWorkday():void
    {
        $this->subWorkdays(1);
    }

    /**
     * Вычесть несколько рабочих дней с текущей даты
     *
     * @param int $count Количество вычитаемых рабочих дней
     */
    public function subWorkdays(int $count):void
    {
        $count = abs($count);
        while ($count > 0) {
            $this->subDay();

            if ($this->isWorkday()) {
                $count--;
            }
        }
    }

    /**
     * True - рабочий день, false - выходной
     *
     * @return bool
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function isWorkday(): bool
    {
        $params=[
            'filter'=>[
                'DATE'=> Type\Date::createFromTimestamp($this->timestamp)
            ],
            'limit'=>1
        ];
        $rsItem=CalendarTable::getList($params);
        $result=$rsItem->fetchAll();

        return empty($result);
    }

    /**
     * Вычисляется разница в рабочих днях между двумя датами.
     * Возвращается число рабочих дней.
     *
     * @param WorkCalendar $carbon Дата, с которой надо
     * @return int
     * @throws \Bitrix\Main\ArgumentException
     * @throws \Bitrix\Main\ObjectPropertyException
     * @throws \Bitrix\Main\SystemException
     */
    public function diffInWorkdays(WorkCalendar $carbon): int
    {
        $workdaysDiffCount = 0;

        $daysDiffCount = $this->diffInDays($carbon, false);
        if ($daysDiffCount > 0) {
            // $carbon больше текущей даты
            $initialDate = $this->copy();
            $revert = false;
        } else {
            // $carbon меньше текущей даты
            $initialDate = $carbon->copy();
            $revert = true;

            $daysDiffCount = abs($daysDiffCount);
        }

        for ($i = 1; $i <= $daysDiffCount; $i++) {
            $initialDate->addDay();
            if ($initialDate->isWorkday()) {
                $workdaysDiffCount++;
            }
        }

        if ($revert) {
            $workdaysDiffCount = -$workdaysDiffCount;
        }

        return $workdaysDiffCount;
    }
}