<?
use \Bitrix\Main\Localization\Loc,
	\Bitrix\Main\ModuleManager,
	\Bitrix\Main\EventManager,
	\Bitrix\Main\Application;

Loc::loadMessages(__FILE__);

if(class_exists("tbwp_productivecalendar")) return;

Class tbwp_productivecalendar extends CModule
{

	function __construct(){
		$arModuleVersion = array();
		include(dirname(__FILE__)."/version.php");

		$this->MODULE_ID = 'tbwp.productivecalendar';
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_MODULE_NAME");
		$this->MODULE_DESCRIPTION = Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_MODULE_DESCRIPTION");

		$this->PARTNER_NAME = Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_PARTNER_NAME");
	}


	function InstallDB()
	{
        global $DB;
        $DB->runSQLBatch($this->GetPath(). '/install/db/' . mb_strtolower($DB->type) . '/install.sql');
	}

	function UnInstallDB($arParams = Array())
	{
        global $DB;
        if(array_key_exists("savedata", $arParams) && $arParams["savedata"] != "Y") {
            $DB->runSQLBatch($this->GetPath() . '/install/db/' . mb_strtolower($DB->type) . '/uninstall.sql');
        }
	}

	function InstallEvents()
	{
		$eventManager = EventManager::getInstance();
        $eventManager->registerEventHandler(
            "main",
            "OnBuildGlobalMenu",
            $this->MODULE_ID,
            'Tbwp\ProductiveCalendar\Main',
            'OnBuildGlobalMenuHandler'
        );
        $eventManager->registerEventHandler(
            "sale",
            "onSaleDeliveryRestrictionsClassNamesBuildList",
            $this->MODULE_ID,
            "Tbwp\ProductiveCalendar\Main",
            "onSaleDeliveryRestrictionsClassNamesBuildList"
        );
        $eventManager->registerEventHandler(
            "sale",
            "onSalePaySystemRestrictionsClassNamesBuildList",
            $this->MODULE_ID,
            "Tbwp\ProductiveCalendar\Main",
            "onSalePaySystemRestrictionsClassNamesBuildList"
        );

		return true;
	}

	function UnInstallEvents()
	{
		$eventManager = EventManager::getInstance();
		$eventManager->unRegisterEventHandler(
            "main",
            "OnBuildGlobalMenu",
            $this->MODULE_ID,
            'Tbwp\ProductiveCalendar\Main',
            'OnBuildGlobalMenuHandler'
		);
        $eventManager->unRegisterEventHandler(
            "sale",
            "onSaleDeliveryRestrictionsClassNamesBuildList",
            $this->MODULE_ID,
            "Tbwp\ProductiveCalendar\Main",
            "onSaleDeliveryRestrictionsClassNamesBuildList"
        );
        $eventManager->unRegisterEventHandler(
            "sale",
            "onSalePaySystemRestrictionsClassNamesBuildList",
            $this->MODULE_ID,
            "Tbwp\ProductiveCalendar\Main",
            "onSalePaySystemRestrictionsClassNamesBuildList"
        );

		return true;
	}


	function InstallFiles() {
        CopyDirFiles( $this->GetPath().'/install/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin', true );
        CopyDirFiles( $this->GetPath()."/install/themes/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/", true, true );

	}

	function UnInstallFiles()	{
        DeleteDirFiles( $this->GetPath().'/install/admin', $_SERVER['DOCUMENT_ROOT'].'/bitrix/admin' );
        //DeleteDirFiles( $this->GetPath()."/install/themes/.default/", $_SERVER["DOCUMENT_ROOT"]."/bitrix/themes/.default" );
	}

	function GetPath($notDocumentRoot = false){
		if ($notDocumentRoot){
			return str_ireplace(Application::getDocumentRoot(), '', dirname(__DIR__));
		}else{
			return dirname(__DIR__);
		}
	}

	function DoInstall()
	{
		global $APPLICATION;

        if(CheckVersion(ModuleManager::getVersion("main"), "16.00.28")){

			$this->InstallDB();
			$this->InstallFiles();
			$this->InstallEvents();
            ModuleManager::registerModule($this->MODULE_ID);
        }else{

            $APPLICATION->ThrowException(Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_INSTALL_ERROR_VERSION"));
        }
	}

	function DoUninstall()
	{
        global $APPLICATION, $step;
        $step = IntVal($step);
        if($step<2)
        {
            $APPLICATION->IncludeAdminFile(Loc::getMessage("INSTALL_UNINSTALL"), $this->GetPath()."/install/unstep1.php");
        }
        elseif($step==2) {
            $this->UnInstallFiles();
            $this->UnInstallDB(array(
                "savedata" => $_REQUEST["savedata"],
            ));
            $this->UnInstallEvents();

            \Bitrix\Main\ModuleManager::unRegisterModule($this->MODULE_ID);

            $APPLICATION->IncludeAdminFile(Loc::getMessage("INSTALL_UNINSTALL"), $this->GetPath() . "/install/unstep.php");
        }
	}
}