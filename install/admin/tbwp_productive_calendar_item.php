<?
$path = '/tbwp.productivecalendar/admin/tbwp_productive_calendar_item.php';
if (file_exists($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules" . $path)) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules" . $path);
} elseif (file_exists($_SERVER["DOCUMENT_ROOT"] . "/local/modules" . $path)) {
    require_once($_SERVER["DOCUMENT_ROOT"] . "/local/modules" . $path);
}
?>