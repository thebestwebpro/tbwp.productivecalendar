<?php
namespace Tbwp\ProductiveCalendar;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

Loader::registerAutoLoadClasses(
    null,
    array(
        __NAMESPACE__.'\\Restriction\\RestrictionByProductiveCalendar'=> 'lib/restrictionbyproductivecalendar.php'
    )
);

class Main {

    const moduleId='tbwp.productivecalendar';

    /**
     * @param bool $absolute
     * @return string
     * @throws \Bitrix\Main\IO\InvalidPathException
     */
    public static function GetPathModule(bool $absolute=false):string
    {
        $path=str_replace(\Bitrix\Main\Application::getDocumentRoot(),"",__DIR__);
        $path=str_replace(str_replace("/","\\",\Bitrix\Main\Application::getDocumentRoot()),"",$path);
        $localPath=substr($path,0,strrpos($path,self::moduleId )).self::moduleId;
        return $absolute ? \Bitrix\Main\IO\Path::normalize(\Bitrix\Main\Application::getDocumentRoot().$localPath) : \Bitrix\Main\IO\Path::normalize($localPath);
    }

    /**
     * @param array $arGlobalMenu
     * @param array $arModuleMenu
     * @return void
     */
    public static function OnBuildGlobalMenuHandler(array &$arGlobalMenu, array &$arModuleMenu):void
    {
        if (!isset($arGlobalMenu['global_menu_tbwp'])) {
            $arGlobalMenu['global_menu_tbwp'] = [
                'menu_id'   => 'tbwp',
                'text'      => Loc::getMessage('TBWP_GLOBAL_MENU'),
                'title'     => Loc::getMessage('TBWP_GLOBAL_MENU' ),
                'sort'      => 1000,
                'items_id'  => 'global_menu_tbwp_items',
                "icon"      => "",
                "page_icon" => "",
            ];
        }

        $items=array(
            array(
                "text" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_MENU_LIST"),
                "url" => "tbwp_productive_calendar_list.php" . "?lang=".LANGUAGE_ID,
                "title" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_MENU_LIST"),
                "items_id" => "menu_tbwp_productive_calendar_list",
                'more_url' => array(
                    'tbwp_productive_calendar_item.php'
                ),
            ),
        );

        $aMenu = array(
            "parent_menu" => 'global_menu_tbwp',
            "section" => 'tbwp.productivecalendar',
            "sort" => 350,
            "text" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_MENU_TITLE"),
            "title" => Loc::getMessage("TBWP_PRODUCTIVE_CALENDAR_MENU_TITLE"),
            "icon" => "tbwp_productive_calendar_menu_icon",
            "items_id" => "menu_tbwp.prductivecalendar",
            "dynamic" => true,
            'items' => $items,
            'more_url' => array(
                'tbwp_productive_calendar_item.php',
                'tbwp_productive_calendar_list.php',
                'tbwp_productive_calendar_update.php',
            ),
        );

        $arGlobalMenu['global_menu_tbwp']['items']['tbwp.productivecalendar'] = $aMenu;
    }

    /**
     * @return \Bitrix\Main\EventResult
     * @throws \Bitrix\Main\IO\InvalidPathException
     */
    public static function onSaleDeliveryRestrictionsClassNamesBuildList() : \Bitrix\Main\EventResult
    {

        return new \Bitrix\Main\EventResult(
            \Bitrix\Main\EventResult::SUCCESS,
            array(
                __NAMESPACE__.'\Restriction\RestrictionByProductiveCalendar'=>self::GetPathModule().'/lib/restrictionbyproductivecalendar.php',
            )
        );
    }

    /**
     * @return \Bitrix\Main\EventResult
     * @throws \Bitrix\Main\IO\InvalidPathException
     */
    public static function onSalePaySystemRestrictionsClassNamesBuildList():\Bitrix\Main\EventResult
    {

        return new \Bitrix\Main\EventResult(
            \Bitrix\Main\EventResult::SUCCESS,
            array(
                __NAMESPACE__.'\Restriction\RestrictionByProductiveCalendar'=>self::GetPathModule().'/lib/restrictionbyproductivecalendar.php',

            )
        );
    }

}